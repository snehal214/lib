/*
 * Public API Surface of form-control
 */

export * from './lib/form-control.module';

// components
export * from './lib/text-box/text-box.component'
export * from './lib/password/password.component';
export * from './lib/telephone/telephone.component';
export * from './lib/checkbox/checkbox.component';
export * from './lib/radio/radio.component';
export * from './lib/text-area/text-area.component';