import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioComponent implements OnInit {

  @Input() name : string;
  @Input() value : string;
  @Input() checked : string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
