import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() name : string;
  @Input() value : string;
  @Input() checked : string;

  constructor() { }

  ngOnInit(): void {
  }

}
