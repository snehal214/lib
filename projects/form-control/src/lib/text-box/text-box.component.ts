import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.css']
})
export class TextBoxComponent implements OnInit {

  @Input() name : string;
  @Input() placeholder : string;
  @Input() value : string;

  constructor() { }

  ngOnInit(): void {
  }

}
