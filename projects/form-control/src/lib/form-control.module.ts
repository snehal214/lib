import { NgModule } from '@angular/core';
import { TextBoxComponent } from './text-box/text-box.component';
import { TelephoneComponent } from './telephone/telephone.component';
import { PasswordComponent } from './password/password.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { RadioComponent } from './radio/radio.component';
import { TextAreaComponent } from './text-area/text-area.component';


@NgModule({
  declarations: [ 
    TextBoxComponent, 
    TelephoneComponent, 
    PasswordComponent, 
    CheckboxComponent, 
    RadioComponent,
    TextAreaComponent
  ],
  imports: [
  ],
  exports: [
    TextBoxComponent, 
    TelephoneComponent, 
    PasswordComponent, 
    CheckboxComponent, 
    RadioComponent,
    TextAreaComponent
  ]
})
export class FormControlModule { }
