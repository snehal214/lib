import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.css']
})
export class TextAreaComponent implements OnInit {

  @Input() name : string;
  @Input() placeholder : string;
  @Input() value : string;

  constructor() { }

  ngOnInit(): void {
  }

}
