import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  @Input() name : string;
  @Input() placeholder : string;
  @Input() value : string;

  constructor() { }

  ngOnInit(): void {
  }

}
