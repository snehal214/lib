import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'form-control-telephone',
  templateUrl: './telephone.component.html',
  styleUrls: ['./telephone.component.css']
})
export class TelephoneComponent implements OnInit {

  @Input() name : string;
  @Input() placeholder : string;
  @Input() value : string;

  constructor() { }

  ngOnInit(): void {
  }

}
