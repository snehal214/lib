# Lib

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.


## Code scaffolding

Run `ng generate library library-name` to generate a new library. All the libraries will be located under projects folder.

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

To expose any new `component|directive|pipe|service|class|guard|interface|enum|module` of the library :
    1. Add it in Exports of specific Library's Module - `library-name.module.ts`
    2. Add its path in `public-api.ts`


## Build

Run `ng build library-name` | `npm run lib:build` to build the library. The build artifacts will be stored in the `dist/library-name` directory. 

Build everytime after any changes made in the library.


## Package

Run `cd dist/library-name && npm pack` | `npm run lib:package` to create a package of the library. The package artifacts will be stored in the `dist/library-name` directory.

`library-name-version.tgz` will be created.

Package everytime after any changes made in the library.


## Library Usage

You can use this library in your project by giving path to the package file generate
`npm i library-name-version.tgz --save`

If any such issue is faced -
`Cannot read property 'bindingStartIndex' of null when using Library`
This occurs due to issue of library with npm link - 

Solution 1;
    Upload this library to npm registry and them install it.

Solution 2:
    1. Copy the library dist folder in node_modules 
    2. Update path in package.json/ package-lock.json - `node_modules/library-name`


